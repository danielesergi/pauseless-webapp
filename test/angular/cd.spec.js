describe('Controller singolo mix', () => {

   let expected = "";
   var $scope, $controller, $routeParams;
   let mockValue = {
      "$":{"name":"da 1"},
      "song":[
         {"$":{"track":"1","bpm":"140","artist":"mad maxx","wav":"wav","key":"B+","title":"B+ 140","used":1,"used_mixes":["testone"]},"pause":["3.20-2.00"]},
         {"$":{"track":"2","bpm":"140","artist":"alchemix","wav":"wav","key":"F#+","title":"F#+ 140","used":1,"used_mixes":["testone"]},"pause":[""]},
         {"$":{"track":"3","bpm":"140","artist":"aphid","wav":"wav","key":"C#+","title":"C#+ 140","used":0},"pause":["1.00-0.22"]}]};
   let keyMockValue = {"minor":[{"key":"G#-","position":1,"mark":"a"},{"key":"D#-","position":2,"mark":"a"},{"key":"A#-","position":3,"mark":"a"},{"key":"F-","position":4,"mark":"a"},{"key":"C-","position":5,"mark":"a"},{"key":"G-","position":6,"mark":"a"},{"key":"D-","position":7,"mark":"a"},{"key":"A-","position":8,"mark":"a"},{"key":"E-","position":9,"mark":"a"},{"key":"B-","position":10,"mark":"a"},{"key":"F#-","position":11,"mark":"a"},{"key":"C#-","position":12,"mark":"a"}],"major":[{"key":"B+","position":1,"mark":"b"},{"key":"F#+","position":2,"mark":"b"},{"key":"C#+","position":3,"mark":"b"},{"key":"G#+","position":4,"mark":"b"},{"key":"D#+","position":5,"mark":"b"},{"key":"A#+","position":6,"mark":"b"},{"key":"F+","position":7,"mark":"b"},{"key":"C+","position":8,"mark":"b"},{"key":"G+","position":9,"mark":"b"},{"key":"D+","position":10,"mark":"b"},{"key":"A+","position":11,"mark":"b"},{"key":"E+","position":12,"mark":"b"}]};
   let unsavedStateService = {
      unsavedState : false,
      unsaved : function() {
         this.unsavedState = true;
      }
   };

   beforeEach(() => {
      module('ngMockE2E');
      module('pauseless');
   });

   beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      $controller = $injector.get('$controller');
      $http = $injector.get('$httpBackend');
      $routeParams = $injector.get('$routeParams');
      $scope = $rootScope.$new();
      $httpBackend.when('GET', /^\/views\//).passThrough();

      //dipendenze : 
      //$rootScope, $scope, $http, $location, $routeParams, FileUploader, songsFactory, unsavedState
   }));

   beforeEach(inject(function (_$http_, _$q_) {
      $http = _$http_;
      $q = _$q_;
   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

   afterEach(() => {
      //expected = "";
   });

   it('istanzio il controller e controllo che chiami la route dei cd e delle keys', () => {
      $httpBackend.expectGET('/json/cd/da 1').respond(200, mockValue);
      $httpBackend.expectGET('/json/keys').respond(200, keyMockValue);

      $routeParams.id = 'da 1';
      $controller("cd", { $rootScope: $scope, $scope: $scope });

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect($scope.songs).toBeDefined();
      expect($scope.songs).toEqual(mockValue.song);
      expect($scope.keys).toBeDefined();
      //expect($scope.mix_description).toBeDefined();
      //expect($scope.mixes).toEqual(mockValue);
   });

   /*it('istanzio il controller e provo ad aggiungere una canzone', () => {
      $httpBackend.expectGET('/json/cd/da 1').respond(200, mockValue);
      $httpBackend.expectGET('/json/keys').respond(200, keyMockValue);
      //$httpBackend.expectPOST('/cd/song/add').respond(200, mockValue);

      $routeParams.id = 'da 1';
      $controller("cd", { $rootScope: $scope, $scope: $scope, unsavedState: unsavedStateService });
      expect(unsavedStateService.unsavedState).toBeFalsy();
      $scope.addSong({});

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect(unsavedStateService.unsavedState).toBeTruthy();
   });*/

});