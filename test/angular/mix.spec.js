describe('Controller singolo mix', () => {

   let expected = "";
   var $scope, $controller, $routeParams;
   let mockValue = {
      "$":{"name":"testone","description":"","bpm":"144"},
      "song":[
         {"$":{"path":"da 1/1","note":"","song":"mad maxx B+ 140 (140)","key":"B+","bpm":"140"}},
         {"$":{"type":"ext","title":"batte","artist":"tua madre","bpm":"144","key":"B+","note":"non metterla fa cagare","song":"tua madre batte (144)"}},
         {"$":{"path":"da 2/9","note":"","song":"avalon G+ 141 (141)","key":"G+","bpm":"141"}},
         {"$":{"path":"da 3/10","note":"","song":"dickster D+ 142 (142)","key":"D+","bpm":"142"}},
         {"$":{"type":"nota","note":"elemento a se stante, porcoddio"}}
      ]};

   beforeEach(() => {
      module('ngMockE2E');
      module('pauseless');
   });

   beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      $controller = $injector.get('$controller');
      $http = $injector.get('$httpBackend');
      $routeParams = $injector.get('$routeParams');
      $scope = $rootScope.$new();
      $httpBackend.when('GET', /^\/views\//).passThrough();

      //dipendenze : 
      //['$rootScope','$scope','$http','$location','$routeParams'
   }));

   beforeEach(inject(function (_$http_, _$q_) {
      $http = _$http_;
      $q = _$q_;
   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

   afterEach(() => {
      //expected = "";
   });

   it('istanzio il controller e controllo che chiami la route dei mix', () => {
      $httpBackend.expectGET('/json/mix/da 1').respond(200, mockValue);

      $routeParams.id = 'da 1';
      $controller("mix", { $rootScope: $scope, $scope: $scope });

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect($scope.songs).toBeDefined();
      expect($scope.mix_name).toBeDefined();
      expect($scope.mix_bpm).toBeDefined();
      expect($scope.mix_description).toBeDefined();
      //expect($scope.mixes).toEqual(mockValue);
   });

});