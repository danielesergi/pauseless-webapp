describe('Controller lista cd', () => {

   let expected = "";
   var $scope, $controller;
   let mockValue = [{
      "$": {
         "name": "da 1"
      },
      "song": [
         {
            "$": {
               "track": "1",
               "bpm": "140",
               "artist": "mad maxx",
               "wav": "wav",
               "key": "B+",
               "title": "B+ 140",
               "used": 1,
               "used_mixes": [
                  "testone"
               ]
            },
            "pause": [
               "3.20-2.00"
            ]
         },
         {
            "$": {
               "track": "2",
               "bpm": "140",
               "artist": "alchemix",
               "wav": "wav",
               "key": "F#+",
               "title": "F#+ 140",
               "used": 1,
               "used_mixes": [
                  "testone"
               ]
            },
            "pause": [
               ""
            ]
         }
      ]
   }];
   let cd2 = {"$":{"name":"da 2"},"song":[]};

   beforeEach(() => {
      expected = "Hello World";
      module('ngMockE2E');
      module('pauseless');
   });

   beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      $controller = $injector.get('$controller');
      $http = $injector.get('$httpBackend');
      $scope = $rootScope.$new();
      $httpBackend.when('GET', /^\/views\//).passThrough();

      //dipendenze : 
      //['$rootScope','$scope','$http','$location','$routeParams','songsFactory','unsavedState'
   }));

   beforeEach(inject(function (_$http_, _$q_) {
      $http = _$http_;
      $q = _$q_;
   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

   afterEach(() => {
      expected = "";
   });

   it('istanzio il controller e controllo che chiami la route dei cd', () => {
      $httpBackend.expectGET('/json/cd').respond(200, mockValue);

      $controller("cds", { $rootScope: $scope, $scope: $scope });

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect($scope.cds).toBeDefined();
      expect($scope.cds).toEqual(mockValue);
   });

   it('provo ad aggiungere un cd', () => {
      $httpBackend.expectGET('/json/cd').respond(200, mockValue);
      $httpBackend.expectPOST('/cd/add').respond(200, mockValue);

      $controller("cds", { $rootScope: $scope, $scope: $scope });

      $scope.cdName = "da 2";
      $scope.addCd();

      $httpBackend.flush();

      expect($scope.cds).toBeDefined();
      expect($scope.cdName).toEqual("");//il valore viene ripulito dalla funzione di add
   });
});