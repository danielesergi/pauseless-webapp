describe('Controller mix temporanei', () => {

   let expected = "";
   var $scope, $controller, $routeParams, modulo;
   let mockValue = ["tmp_mixdiprova.xml"];
   let unsavedStateService = {
      unsavedState : false,
      unsaved : function() {
         this.unsavedState = true;
      }
   };

   beforeEach(() => {
      module('ngMockE2E');
      module('pauseless');


      /*modulo = angular.module("pauseless");
      console.log('modulo', modulo);
      console.log('requires', modulo.value().requires);
      console.log('value', modulo.value());*/
   });

   beforeEach(inject(function ($injector) {
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      $controller = $injector.get('$controller');
      $http = $injector.get('$httpBackend');
      $scope = $rootScope.$new();
      $httpBackend.when('GET', /^\/views\//).passThrough();

      //dipendenze : 
      //['$rootScope','$scope','$http','unsavedState'
   }));

   beforeEach(inject(function (_$http_, _$q_) {
      $http = _$http_;
      $q = _$q_;

   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

   afterEach(() => {
      //expected = "";
   });

   it('istanzio il controller e controllo che chiami la route dei mix temporanei', () => {
      //expect(modulo).not.toEqual(null);
      $httpBackend.expectGET('/json/tmpmix').respond(200, mockValue);

      $controller("tmpmixes", { $rootScope: $scope, $scope: $scope });
      //console.log('$controller', $controller());

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect($scope.mixes).toBeDefined();
      expect($scope.mixes).toEqual(mockValue);
   });

   it('istanzio il controller e provo a fare un dump per controllare il servizio unsavedState', () => {
      //expect(modulo).not.toEqual(null);
      let mixName = 'test';
      $httpBackend.expectGET('/json/tmpmix').respond(200, mockValue);
      $httpBackend.expectGET('/tmpmix/save/'+mixName).respond(200, '');
      $httpBackend.expectGET('/tmpmix/remove/'+mixName).respond(200, '');
      $httpBackend.expectGET('/json/tmpmix').respond(200, mockValue);

      $controller("tmpmixes", { $rootScope: $scope, $scope: $scope, unsavedState: unsavedStateService });
      expect(unsavedStateService.unsavedState).toBeFalsy();
      $scope.dumpMix(mixName);

      //$rootScope.$digest(); dicono sia necessario per risolvere le promises

      $httpBackend.flush();

      expect(unsavedStateService.unsavedState).toBeTruthy();
   });

});