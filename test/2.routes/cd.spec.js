const chai = require('chai');
const expect = chai.expect;
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader');
const _ = require('underscore');

// file da testare
const rcd = require(process.env.PWD + '/server/routes/cd');


describe('route cd, operazioni con utente test : get', () => {

  let req = {};
  let response = {};
  let res = {
    status: function (n) { console.log('mandato stato http', n); return this; },
    send: function (payload) { response = payload; }
  };

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      rcd.get(req, res);
      done()
    }, 'test');
  });

  describe('controlli get', () => {
    it('get di tutti i cd non è undefined', () => {
      //console.log(JSON.stringify(response))
      expect(response).to.not.be.undefined;
    });

    it('get di tutti i cd ha 6 cd con utente test', () => {
      //console.log(JSON.stringify(response))
      expect(response.length).to.equal(6);
    });

  });
});

describe('route cd, operazioni con utente test : get_cd', () => {

  let req = {};
  let response = {};
  let res = {
    status: function (n) { console.log('mandato stato http', n); return this; },
    send: function (payload) { response = payload; }
  };

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      req.params = { id: "da 1" };
      rcd.get_cd(req, res);
      done()
    }, 'test');
  });

  describe('controlli get', () => {
    it('get di cd "da 1" non è undefined', () => {
      expect(response).to.not.be.undefined;
    });

    it('get di cd "da 1" ha 24 canzoni con utente test', () => {
      expect(response.song.length).to.equal(24);
    });

  });
});

describe('route cd, operazioni con utente test : add_cd', () => {

  let req = {};
  let response = {};
  let res = {
    status: function (n) { console.log('mandato stato http', n); return this; },
    send: function (payload) { response = payload; }
  };

  before(function (done) {
    // runs before all tests in this block
    // prima cosa carico i files con utente test
    xml_data_reader.load(req, {}, function () {
      done()
    }, 'test');
  });

  describe('controlli e manipolazione lista cd', () => {
    it('get di tutti i cd non è undefined', () => {
      rcd.get(req, res);
      expect(response).to.not.be.undefined;
    });

    it('get di tutti i cd ha 6 cd con utente test', () => {
      expect(response.length).to.equal(6);
    });

    it('aggiungo cd con utente test', () => {
      //req.body = _.clone(example_cd);
      //req.body.song = [];
      req.body = {"$":{"name":"da 7"},"song":[]};
      rcd.add_cd(req, res);
      //rcd.get(req, res);
      console.log(JSON.stringify(response))
      expect(response.length).to.equal(7);
    });

    it('aggiungo canzone ad un cd esistente con utente test', () => {
      req.params = { id: "da 7" };
      rcd.get_cd(req, res);
      expect(response.song.length).to.equal(0);

      //req.body = _.clone(example_cd);
      //delete req.body.song[0].track;//la canzone la mando senza numero di traccia
      req.body = {"$":{"name":"da 7"},"song":[{"$":{"artist":"artista","track":1,"title":"titolo","wav":"wav","bpm":"120","key":""},"pause":["-"]}]};
      rcd.add_song(req, res);
      //console.log(JSON.stringify(response))
      //rcd.get_cd(req, res);
      expect(response.song.length).to.equal(1);
    });

    it('controllo numerazione automatica canzoni con utente test', () => {
      //questo test si rompe usando i valori di default che mi sono scritto in fondo al file...funziona invece passandogli le stesse cose dell'interfaccia
      req.body = {"$":{"name":"da 7"},"song":[{"$":{"artist":"artista due","title":"titolo due","wav":"wav","bpm":"120","key":""},"pause":["-"]}]};
      rcd.add_song(req, res);

      //req.params = { id: "da 7" };
      //rcd.get_cd(req, res);
      //console.log(JSON.stringify(response))
      expect(response.song.length).to.equal(2);
      expect(response.song[0].$.track).to.equal(1);
      expect(response.song[1].$.track).to.equal(2);
    });

    it('modifica traccia con utente test', () => {
      req.params = { id: "da 7" };
      rcd.get_cd(req, res);
      //console.log(JSON.stringify(response))
      expect(response.song[0].$.artist).to.equal('artista');
      expect(response.song[0].$.title).to.equal('titolo');

      req.body = {"cd":"da 7","song":{"$":{"artist":"artista modificato","track":1,"title":"titolo modificato","wav":"wav","bpm":"120","key":""},"pause":["-"]}};
      rcd.update_song(req, res);

      //req.params = { id: "da 7" };
      //rcd.get_cd(req, res);
      //console.log(JSON.stringify(response))
      expect(response.song[0].$.artist).to.equal('artista modificato');
      expect(response.song[0].$.title).to.equal('titolo modificato');
    });

    it('swappo traccia 1 e traccia 2', () => {
      req.body = {"cd":"da 7","from":1, "to":2};
      rcd.swap_song(req, res);

      //req.params = { id: "da 7" };
      //rcd.get_cd(req, res);
      //console.log(JSON.stringify(response))
      expect(response.song[1].$.artist).to.equal('artista modificato');
      expect(response.song[1].$.title).to.equal('titolo modificato');
      expect(response.song[0].$.artist).to.equal('artista due');
      expect(response.song[0].$.title).to.equal('titolo due');
      expect(response.song[0].$.track).to.equal(1);
      expect(response.song[1].$.track).to.equal(2);
    });

    it('rimuovo la prima traccia', () => {
      req.body = {"cd":"da 7","song":{"$":{"track":1},"pause":["-"]}};
      rcd.delete_song(req, res);

      //req.params = { id: "da 7" };
      //rcd.get_cd(req, res);
      //console.log(JSON.stringify(response))
      expect(response.song.length).to.equal(1);
      expect(response.song[0].$.track).to.equal(1);
      expect(response.song[0].$.artist).to.equal('artista modificato');
      expect(response.song[0].$.title).to.equal('titolo modificato');
    });

  });//
});




var example_song = {
    "$":
    {
      "track": "1",
      "bpm": "140", "artist": "mad maxx", "wav": "wav", "key": "B+", "title": "B+ 140", "used": 1,
      "used_mixes": ["testone"]
    }, "pause": ["3.20-2.00"]
  }

var example_cd = {
    "$":
      { "name": "da 7" },
    "song": [
      {
        "$":
        {
          "track": 1, 
          "bpm": "140", "artist": "mad maxx", "wav": "wav", "key": "B+", "title": "B+ 140",
          "used": 1, "used_mixes": ["testone"]
        }, "pause": ["3.20-2.00"]
      }]
  }
