const chai = require('chai');
const expect = chai.expect;
const xml_data_reader = require(process.env.PWD + '/server/middleware/xml_data_reader');
const _ = require('underscore');

// file da testare
const rk = require(process.env.PWD + '/server/routes/keys');


describe('route keys : get', () => {

    let req = {};
    let response = {};
    let res = {
        status: function (n) { console.log('mandato stato http', n); return this; },
        send: function (payload) { response = payload; }
    };

    /*before(function (done) {
      // runs before all tests in this block
      // prima cosa carico i files con utente test
      xml_data_reader.load(req, {}, function () {
        rcd.get(req, res);
        done()
      }, 'test');
    });*/

    describe('controlli get', () => {
        it('get delle keys non è undefined', () => {
            rk.get(req, res);
            expect(response).to.not.be.undefined;
        });

        it('get delle keys ha due posizioni (major e minor)', () => {
            expect(_.keys(response).length).to.equal(2);
            expect(response.major).to.not.be.undefined;
            expect(response.minor).to.not.be.undefined;
        });

    });
});

/*describe('route keys : get_songs_by_key', () => {

    let req = {};
    let response = {};
    let res = {
        status: function (n) { console.log('mandato stato http', n); return this; },
        send: function (payload) { response = payload; }
    };

    before(function (done) {
      // runs before all tests in this block
      // prima cosa carico i files con utente test
      xml_data_reader.load(req, {}, function () {
        done()
      }, 'test');
    });

    describe('controlli get', () => {
        it('get delle keys non è undefined', () => {
            rk.get_songs_by_keys(req, res);
            expect(response).to.not.be.undefined;
        });

        //questo metodo forse non è usato, metto una nota anche nel codice
        it('get delle keys ha due posizioni (major e minor)', () => {
            console.log(response);
            expect(_.keys(response).length).to.equal(2);
            expect(response.major).to.not.be.undefined;
            expect(response.minor).to.not.be.undefined;
        });

    });
});*/