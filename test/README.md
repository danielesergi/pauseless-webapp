per fare i test nelle cartelle 1 e 2 servono : gulp, chai, gulp-mocha

link utili : 
https://medium.com/building-ibotta/testing-arrays-and-objects-with-chai-js-4b372310fe6d

per fare i test sulle api ho usato gulp, chai, chai-http, gulp-mocha

per fare i test su angular ho installato una madonna di roba. Ricordarsi che angular-mocks è una vecchia versione, per andare dietro al mio vecchio angular

Unable to find a suitable version for angular, please choose one:
    1) angular#~1.3.9 which resolved to 1.3.20 and is required by pauseless-webapp 
    2) angular#1.3.20 which resolved to 1.3.20 and is required by angular-route#1.3.20 
    3) angular#1.4.6 which resolved to 1.4.6 and is required by angular-mocks#1.4.6 
    4) angular#^1.1.5 which resolved to 1.7.8 and is required by angular-file-upload#2.5.0 
    5) angular#^1.x which resolved to 1.7.8 and is required by angular-local-storage#0.7.1Prefix the choice with ! to persist it to bower.json

? Answer:: !1
bower angular               resolution Saved angular#~1.3.9 as resolution
bower angular-mocks#1.4.6      install angular-mocks#1.4.6


/*beforeEach(inject(function($rootScope, $controller) {
        $scope = $rootScope.$new();
        $controller("cds", {$scope: $scope});
        console.log('istanziato controller');
        //dipendenze : 
        //['$rootScope','$scope','$http','$location','$routeParams','songsFactory','unsavedState'
    }));*/

    //questo beforEach e quello sopra sono equivalenti, cambia la sintassi
    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get('$httpBackend');
        $rootScope = $injector.get('$rootScope');
        $controller = $injector.get('$controller');
        $scope = $rootScope.$new();

        authRequestHandler = $httpBackend.when('GET', '/json/cd')
                            .respond({userId: 'userX'}, {'A-Token': 'xxx'});

        $controller("cds", {$rootScope: $scope, $scope: $scope});
        console.log('istanziato controller');
        //dipendenze : 
        //['$rootScope','$scope','$http','$location','$routeParams','songsFactory','unsavedState'
    }));


    npm run test fa girare tutto (gulp + gulp karma)
    gulp karma solo la parte karma

    i test sono divisi per ora in 4 sezioni : 
    1.preliminari
        testa funzionalità di librerie di funzioni, in questo caso l'xml data reader
    2.routes
        chiama le varie route aspettandosi risultati noti
    3.api
        testa l'applicazione (devo studiarci ancora)
    angular
        istanzia angular e testa specifiche cose di framework (controller ecc)