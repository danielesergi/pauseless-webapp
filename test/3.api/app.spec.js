const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

//file da testare
const app = require(process.env.PWD + '/app');

describe('start applicazione', () => {

    process.argv[2] = 'test';

    before(function(done) {
        // runs before all tests in this block
        //app.listen(process.env.PORT || 3000);
        done();
    });

    describe('controlli req.xml_cd_list', () => {
        it('get / è definito e non nullo', (done) => {
            chai.request(app)
            .get('/')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.html;
                //res.body.should.be.a('object');
                done();
             });
        });
    });
});
  