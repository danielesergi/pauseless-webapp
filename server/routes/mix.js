var _= require('underscore'),
xml2js = require('xml2js'),
fs = require('fs');

var v_mix_list;

function get_mix (name) {
  //console.log(v_mix_list);
  for(var key in v_mix_list.mixes.mix) {
    if(v_mix_list.mixes.mix[key].$.name == name) {
      //console.log('trovato ' + key);
      return (v_mix_list.mixes.mix[key]);
    }
  }
}

function get_mix_key (name) {
  for(var key in v_mix_list.mixes.mix) {
    if(v_mix_list.mixes.mix[key].$.name == name) {
      return (key);
    }
  }
  return -1;
}

function get_cd (name) {
  //console.log(v_cd_list);
  for(var key in v_cd_list.cds.cd) {
    if(v_cd_list.cds.cd[key].$.name == name) {
      //console.log(v_cd_list.cds.cd[key]);
      return (v_cd_list.cds.cd[key]);
    }
  }
  return([]);
  //return({ '$': { 'name': name } });
}

exports.get= function (req, res) {
	res.status(200).send(req.v_mix_list.mixes.mix);
}

/*exports.getUserMixes= function (req, res) {
    var user = req.params.user;
    //console.log(user)
    xml_data_reader = require('./../middleware/xml_data_reader').set_user_data(req, user, function() {
      res.status(200).send(req.v_mix_list.mixes.mix);
    })
}

exports.getUserMix= function (req, res) {
    var user = req.params.user;
    //console.log(user)
    xml_data_reader = require('./../middleware/xml_data_reader').set_user_data(req, user, function() {
      exports.get_mix(req, res);
    })
}*/

exports.get_near= function (req, res) {
	res.json(req.v_near);
}

exports.get_mix= function (req, res) {
	v_mix_list = req.v_mix_list;
	v_cd_list = req.v_cd_list;
	var mix = get_mix(req.params.id);
  //console.log(JSON.stringify(get_mix(req.params.id)))

	for (var key in mix.song) {
    if (!mix.song[key].$.type || mix.song[key].$.type === 'local') {
	    var path = mix.song[key].$.path.split("/")[0];
	    var song_n = mix.song[key].$.path.split("/")[1];
	    var local_song;
	    var loc_s;

	    local_song = get_cd(path).song[parseInt(song_n)-1];
	    loc_s = local_song.$.artist+' '+local_song.$.title+' ('+local_song.$.bpm+')';

	    //console.log(loc_s);
	    mix.song[key].$.song = loc_s;
		  mix.song[key].$.key = local_song.$.key;
		  mix.song[key].$.bpm = local_song.$.bpm;
	    //local_song = '{ "some" : json }';

	    //songs[key].$.song = local_song.$.artist+' '+local_song.$.title+' ('+local_song.$.bpm+')';
	    //songs[key].$.song = local_song;
	    //songs[key].$.song = local_song;
    } else if (mix.song[key].$.type === 'ext') {
      var loc_s = mix.song[key].$.artist+' '+mix.song[key].$.title+' ('+mix.song[key].$.bpm+')';
      mix.song[key].$.song = loc_s;
		  mix.song[key].$.key = mix.song[key].$.key;
		  mix.song[key].$.bpm = mix.song[key].$.bpm;
    } else if (mix.song[key].$.type === 'nota') {
      //console.log(mix.song[key].$)
    }
  }

  res.send(mix);
}

let save_xml_mix = function (title, xml, req, res) {
	//var builder = new xml2js.Builder({renderOpts:{pretty:true}});
	fs.writeFile('./public/' + req.g_user + '/tmp_'+title, xml, function (err) {
		if (err) throw err;
			console.log('[INFO] file '+title+' salvato!');
  });
  res.status(200).send("Saved!");
}

//l'app usa questa route per salvare un mix temporaneo
exports.save_json_mix= function (req, res) {
	var title = req.body.mix,
		js = req.body.xml;
  
  console.log('save json mix',req.body);
	var builder = new xml2js.Builder({renderOpts:{pretty:true}});
	var xml = builder.buildObject(js);
  //console.log(xml);
  save_xml_mix(title, xml, req, res);
}

exports.save_mix= function (req, res) {
	var title = req.body.mix,
      xml = req.body.xml;
      console.log('save mix',req.body);

      save_xml_mix(title, xml, req, res);
	/*
	//fare backup del file dei mix (non dei cd come qui sotto, che però funziona)
	fs.createReadStream("./public/cd_list.xml").pipe(fs.createWriteStream("./public/cd_list_BCK.xml"));
	//controllare se esiste nella lista dei mix un mix con nome==title
	if (get_mix_key(title) > -1) {
	//se esiste sovrascriverlo
	//v_mix_list.mixes.mix[get_mix_key(title)] = //convertirlo in json
	} else {
	//se non esiste inserirlo in cima
	}
	//salvare mix_list.xml
	*/
	res.send("Saved!");
}


exports.get_tmp_mixes = function (req, res) {
  fs.readdir('./public/' + req.g_user + '/', function (err, files) {
		if (err) throw err;
		console.log('[INFO]', files);
    res.status(200).send(_.filter(files, function(file) { return file.indexOf('tmp_') === 0 }));
	});
}

var lowerCase = function(x) {
    var i, c, txt = "";
    for (i = 0; i < x.length; i++) {
        c = x[i];
        if (i % 2 == 0) {
            c = c.toLowerCase();
        }
        txt += c;
    }
    return txt;
}

var tempMixName = function(x) {
    var txt = x.replace('.xml', '').replace('tmp_', '');
    return txt;
}

//questa sarà ancora usata? secondo me no
/*exports.save_tmp_mix = function (req, res) {
  var name = req.params.name;
  //leggere il contenuto del file e sbatterlo in mezzo agli altri mix
  fs.readFile('./public/' + req.g_user + '/' + name, function (err, data) {
      if (data) {
        var parser = new xml2js.Parser();
        parser.parseString(data, function (err, result) {
            if (err) { console.log(err); res.status(500).send(err); }

            result = result.root;
            result.$.name = lowerCase(tempMixName(name));
            console.log("[INFO]", result);
            if (!req.v_mix_list.mixes.mix)
                req.v_mix_list.mixes.mix = [];

            req.v_mix_list.mixes.mix.unshift(result);
            res.status(200).send();
        });

      } else {
        deferred.reject('Nessun dato caricato per ');
        console.log("[ERROR] nessun dato trovato per " + name + " per " + g_user);
      }
  });
}*/

exports.remove_tmp_mix = function (req, res) {
  var name = req.params.name;
  fs.unlink('./public/' + req.g_user + '/' + name, (err) => {
    if (err) throw err;
    console.log('./public/' + req.g_user + '/' + name + ' was deleted');
    res.status(200).send();
  });
}

exports.dump_mixes= function (req, res) {
  var builder = new xml2js.Builder({renderOpts:{pretty:true}});

	var xml = builder.buildObject(req.v_mix_list);
  //console.log(xml);

	fs.writeFile('./public/' + req.g_user + '/mix_list.xml', xml, function (err) {
		if (err) throw err;
		console.log('[INFO] file mix salvato!');
    res.status(200).send({});
	});
}
