var _= require('underscore'),
    xml2js= require('xml2js'),
    fs = require('fs');

var v_cd_list, v_used_songs;

function get_cd (name) {
  //console.log(v_cd_list);
  for(var key in v_cd_list.cds.cd) {
    if(v_cd_list.cds.cd[key].$.name == name) {
      //console.log(v_cd_list.cds.cd[key]);
      return (v_cd_list.cds.cd[key]);
    }
  }
  return([]);
  //return({ '$': { 'name': name } });
}

function get_cd_key (name) {
  for(var key in v_cd_list.cds.cd) {
    if(v_cd_list.cds.cd[key].$.name == name) {
      return (key);
    }
  }
  return -1;
}


exports.get_range_txt= function (req, res) {
  var from = req.params.from, to = req.params.to, isInRange = false, res = [];
  for(var key in v_cd_list.cds.cd) {
    if(v_cd_list.cds.cd[key].$.name == from) {
      isInRange = true;
    }

    if (isInRange)
      res.push(v_cd_list.cds.cd[key]);

    if(v_cd_list.cds.cd[key].$.name == to) {
      isInRange = false;
    }
  }
  return res;
}

exports.get= function (req, res) {
    //test();
    try {
      if (req.v_cd_list.cds.cd)
          res.status(200).send(req.v_cd_list.cds.cd);
      else
          res.status(200).send([]);
    } catch(e) {
      res.status(200).send([]);//non esistono cd
    }
}

exports.get_cd= function (req, res) {
  v_cd_list = req.v_cd_list;
	let v_used_songs = req.v_used_songs
      id = req.params.id,
      cd = get_cd(id);

	if (cd.song) {
    _.each(cd.song, function(song) {
      if (v_used_songs[id+"/"+song.$.track]) {
				song.$.used = v_used_songs[id+"/"+song.$.track].length;
				song.$.used_mixes = v_used_songs[id+"/"+song.$.track];
			}
			else
				song.$.used = 0;
    })
    //console.log('cd', cd);

	} else
        	cd.song = [];

	res.status(200).send(cd);
}

exports.get_cd_xml= function (req, res) {
    v_cd_list = req.v_cd_list;
    var cd = get_cd(req.params.id);

    var builder = new xml2js.Builder({rootName: 'cd', headless: true});
    var xml = builder.buildObject(cd);
    res.status(200).send(xml);
}

exports.add_cd= function (req, res) {
    if (!req.v_cd_list.cds.cd)
        req.v_cd_list.cds.cd = [];

    req.v_cd_list.cds.cd.push(req.body);
    res.status(200).send(req.v_cd_list.cds.cd);
}

exports.add_song= function (req, res) {
    v_cd_list = req.v_cd_list;
    var cd = get_cd(req.body.$.name);

    //if (!req.v_cd_list.cds.cd[cdk].song) req.v_cd_list.cds.cd[cdk].song = [];
    if (!cd.song) cd.song = [];

    for (i in req.body.song) {
      req.body.song[i].$.track = cd.song.length + 1;//numerazione automatica
      cd.song.push(req.body.song[i]);
    }
    rebuild_track_order(cd.song);

    res.status(200).send(cd);

}

exports.update_song= function (req, res) {
    v_cd_list = req.v_cd_list;
    var cd = get_cd(req.body.cd);

    //req.body.song.$ = _.pick(req.body.song.$, 'artist', 'track', 'title', 'wav', 'bpm', 'key');
    //req.body.song.pause = [];
    //req.body.song.pause.push("-");

    cd.song[parseInt(req.body.song.$.track)-1] = req.body.song;

    //console.log('risultato update song', JSON.stringify(get_cd(req.body.cd)));
    res.status(200).send(cd);

}

function rebuild_track_order (songs) {
  for (s in songs)
    songs[s].$.track = parseInt(s) + 1;
}

exports.delete_song= function (req, res) {
  v_cd_list = req.v_cd_list;
  var cd = get_cd(req.body.cd);
  //console.log(req.v_cd_list.cds.cd[cdk].song)

  cd.song.splice(parseInt(req.body.song.$.track)-1, 1);
  rebuild_track_order(cd.song);
  //console.log(req.v_cd_list.cds.cd[cdk].song)
  res.status(200).send(cd);

}

exports.swap_song= function (req, res) {
  v_cd_list = req.v_cd_list;
  var cd = get_cd(req.body.cd);
  var pos1 = parseInt(req.body.from)-1, pos2 = parseInt(req.body.to)-1;
  //console.log(req.v_cd_list.cds.cd[cdk].song)
  let tmp = cd.song[pos2];
  cd.song[pos2] = cd.song[pos1];
  cd.song[pos1] = tmp;
  //req.v_cd_list.cds.cd[cdk].song[pos1] = req.v_cd_list.cds.cd[cdk].song.splice(pos2, 1, req.v_cd_list.cds.cd[cdk].song[pos1])[0];
  //console.log(req.v_cd_list.cds.cd[cdk].song)
  rebuild_track_order(cd.song);
  res.status(200).send(cd);
}

exports.dump_cds= function (req, res) {
  var builder = new xml2js.Builder({renderOpts:{pretty:true}});
  function _clean(dirty) {
    for (i in dirty.cds.cd)
      for (y in dirty.cds.cd[i])
        for (z in dirty.cds.cd[i][y]) {
          if (dirty.cds.cd[i][y][z].$) //è una canzone
            //console.log("a", dirty.cds.cd[i][y][z].$.used);
            dirty.cds.cd[i][y][z].$ = _.pick(dirty.cds.cd[i][y][z].$, 'track','bpm','artist','wav','key','title');
          //else //è un cd
            //console.log("b", dirty.cds.cd[i][y][z]);
        }

    return dirty;
  }
	var xml = builder.buildObject(_clean(req.v_cd_list));
  console.log(xml);

	fs.writeFile('./public/' + req.g_user + '/cd_list.xml', xml, function (err) {
		if (err) throw err;
		console.log('[INFO] file cd salvato!');
    res.status(200).send({});
	});
}


