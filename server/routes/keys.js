var _ = require('underscore');

var keys = {};
keys.minor = [];
keys.major = [];
keys.minor.push({ key: "G#-", position: 1, mark: "a" });
keys.minor.push({ key: "D#-", position: 2, mark: "a" });
keys.minor.push({ key: "A#-", position: 3, mark: "a" });
keys.minor.push({ key: "F-", position: 4, mark: "a" });
keys.minor.push({ key: "C-", position: 5, mark: "a" });
keys.minor.push({ key: "G-", position: 6, mark: "a" });
keys.minor.push({ key: "D-", position: 7, mark: "a" });
keys.minor.push({ key: "A-", position: 8, mark: "a" });
keys.minor.push({ key: "E-", position: 9, mark: "a" });
keys.minor.push({ key: "B-", position: 10, mark: "a" });
keys.minor.push({ key: "F#-", position: 11, mark: "a" });
keys.minor.push({ key: "C#-", position: 12, mark: "a" });

keys.major.push({ key: "B+", position: 1, mark: "b" });
keys.major.push({ key: "F#+", position: 2, mark: "b" });
keys.major.push({ key: "C#+", position: 3, mark: "b" });
keys.major.push({ key: "G#+", position: 4, mark: "b" });
keys.major.push({ key: "D#+", position: 5, mark: "b" });
keys.major.push({ key: "A#+", position: 6, mark: "b" });
keys.major.push({ key: "F+", position: 7, mark: "b" });
keys.major.push({ key: "C+", position: 8, mark: "b" });
keys.major.push({ key: "G+", position: 9, mark: "b" });
keys.major.push({ key: "D+", position: 10, mark: "b" });
keys.major.push({ key: "A+", position: 11, mark: "b" });
keys.major.push({ key: "E+", position: 12, mark: "b" });

exports.get = function (req, res) {
	res.send(keys);
}

//forse questa funzione non è mai usata
/*exports.get_songs_by_keys = function (req, res) {
	var cds = req.v_cd_list.cds.cd;
	var songs_by_key = {};

	for (var cd in cds) {
		var songs = cds[cd].song;
		for (var song in songs) {
			songs[song].$.path = cds[cd].$.name + "/" + songs[song].$.track;
			if (songs_by_key[songs[song].$.key])
				songs_by_key[songs[song].$.key].push(songs[song].$);
			else {
				songs_by_key[songs[song].$.key] = [];
				songs_by_key[songs[song].$.key].push(songs[song].$);
			}
		}
	}

	return songs_by_key;
}*/
