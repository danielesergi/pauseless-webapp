var _= require('underscore');

exports.get= function (req, res) {
    //console.log(req.params.cd, req.params.track);

    v_cd_list = _.clone(req.v_cd_list);
    v_used_songs = req.v_used_songs;
    var cd = _.find(v_cd_list.cds.cd, (el) => { return el.$.name === req.params.cd })

    if (cd && cd.song) {
        cd = cd.song;
        var song = _.find(cd, (el) => { return el.$.track == req.params.track })
        if (song)
            res.status(200).send(song);
        else res.status(200).send({})

    } else res.status(200).send({})
}

exports.all= function (req, res) {
    var all_cds = _.clone(req.v_cd_list.cds.cd);
    //all_cds = [all_cds[0], all_cds[1]];
    var canzoni = _.flatten(_.map(all_cds, function(cd) { 
        var ret = _.clone(cd.song);
        var result = [];
        _.each(ret, function(canzone) {
            canzone.$.cd = cd.$.name;
            canzone.$.path = cd.$.name + '/' + canzone.$.track;
            canzone.$.pause = canzone.pause;
            delete canzone.pause;
            result.push(canzone.$);
        })
        return result; 
    }));
    //console.log(canzoni);
    res.status(200).send(canzoni);
}

exports.artist= function (req, res) {
    var all_cds = _.clone(req.v_cd_list.cds.cd);
    //all_cds = [all_cds[0], all_cds[1]];
    var canzoni = _.flatten(_.map(all_cds, function(cd) { 
        var ret = _.clone(cd.song);
        var result = [];
        _.each(ret, function(canzone) {
            result.push(canzone.$.artist);
        })
        return result; 
    }));
    //console.log(canzoni);
    res.status(200).send(_.uniq(canzoni).sort());
}
