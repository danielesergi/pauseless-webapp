const gulp = require('gulp');
const mocha = require ('gulp-mocha');
var Server = require('karma').Server;

function test() {
  return gulp.src(['test/1*/*.spec.js', 'test/2*/*.spec.js', 'test/3*/*.spec.js'])
    .pipe(mocha());
}

function single_test () {
  return gulp.src(['test/2*/song.spec.js'])
    .pipe(mocha());
}

gulp.task('single', single_test);

gulp.task('karmaold', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('karma', function(done) {
  Server.start({
      configFile: __dirname + '/karma.conf.js',
      singleRun: true
  }, function() {
      done();
  });
});

// Run our tests as the default task
gulp.task('default', test);

