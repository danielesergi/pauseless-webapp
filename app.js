var express = require('express'),
    bodyParser = require('body-parser'),
//gson = require('express-gson'),
    path = require('path'),
    xml2js = require('xml2js'),
    fs = require('fs'),
    _ = require('underscore'),
    cd = require('./server/routes/cd'),
    mix = require('./server/routes/mix'),
    keys = require('./server/routes/keys'),
    song = require('./server/routes/song'),
    xml_data_reader = require('./server/middleware/xml_data_reader'),
    DOMParser = require('xmldom').DOMParser,
    cors = require('cors'),
    q = require('q'),
    app = express();

var xml_cd_list, xml_mix_list, v_cd_list, v_mix_list, v_used_songs = {};

require('dns').lookup(require('os').hostname(), function (err, add, fam) {
    console.log('[INFO] IP addr supposto: ' + add);
})

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Headers", "Content-Type,x-prototype-version,x-requested-with");
    next();
});

app.use(function (req, res, next) {
  if (!req.v_cd_list)
    req.v_cd_list = v_cd_list;

  if (!req.v_mix_list)
    req.v_mix_list = v_mix_list;

  if (!req.xml_cd_list)
    req.xml_cd_list = xml_cd_list;

  if (!req.xml_mix_list)
    req.xml_mix_list = xml_mix_list;

  if (!req.v_used_songs)
    req.v_used_songs = v_used_songs;

  next();
});

app.use(function (req, res, next) {
    //console.log(process.argv)
    req.g_user = process.argv[2];
    xml_data_reader.load(req, res, next, req.g_user);
});

app.use(function (req, res, next) {
  if (req.v_cd_list)
    v_cd_list = req.v_cd_list;

  if (req.v_mix_list)
    v_mix_list = req.v_mix_list;

  if (req.xml_cd_list)
    xml_cd_list = req.xml_cd_list;

  if (req.xml_mix_list)
    xml_mix_list = req.xml_mix_list;

  if (req.v_used_songs)
    v_used_songs = req.v_used_songs;

  next();
});

/*app.use(function (req, res, next) {
    console.log('[DEBUG] cd list ' + req.v_cd_list.cds.cd.length);
    next();
});*/

app.use(bodyParser({limit: '1mb'}));
app.use(cors());

app.get('/json/cd', cd.get);
app.get('/json/mix', mix.get);
app.get('/json/near', mix.get_near);//non ho idea di cosa sia
app.get('/json/cd/:id', cd.get_cd);
app.get('/json/mix/:id', mix.get_mix);
app.get('/json/tmpmix', mix.get_tmp_mixes);
app.get('/json/keys', keys.get);
app.get('/json/songs', song.all)
app.get('/json/artists', song.artist)
//app.get('/json/song_by_keys', keys.get_songs_by_keys);// a me non sembra usata
app.post('/json/mix', mix.save_json_mix);//questa forma di url mi piace di piu...se abbandono la vecchia app, cambiarle cosi

//app.get('/tmpmix/save/:name', mix.save_tmp_mix);
app.get('/tmpmix/remove/:name', mix.remove_tmp_mix);
app.get('/xml/cd/:id', cd.get_cd_xml);
app.post('/mix/post', mix.save_mix);
app.post('/cd/add', cd.add_cd);
app.post('/cd/post', cd.add_cd);
app.post('/cd/dump', cd.dump_cds);
app.post('/mix/dump', mix.dump_mixes);
app.post('/cd/song/add', cd.add_song);
app.post('/cd/song/update', cd.update_song);
app.post('/cd/song/delete', cd.delete_song);
app.post('/cd/song/swap', cd.swap_song);
app.get('/song/:cd/:track', song.get);
app.get('/txt/cd/:from/:to', cd.get_range_txt);

app.use('/public', express.static(__dirname + '/public'));
app.use('/', express.static(path.join(__dirname, 'client')));

module.exports = app;
