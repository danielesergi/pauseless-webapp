zip -r dist.zip .git client public server .bowerrc .gitignore app.js bower.json package.json start.* update.bat
curl -X POST https://content.dropboxapi.com/2/files/upload \
    --header "Authorization: Bearer stzPnADVgC8AAAAAAABvg7pFLvZ543xabR08fIi82Ps70bkdFSZpgjqxwJrOUeNG" \
    --header "Dropbox-API-Arg: {\"path\": \"/pauseless-dist/webapp-dist.zip\",\"mode\": \"overwrite\",\"autorename\": true,\"mute\": false,\"strict_conflict\": false}" \
    --header "Content-Type: application/octet-stream" \
    --data-binary @dist.zip
