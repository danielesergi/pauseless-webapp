// Karma configuration
// Generated on Thu Mar 14 2019 18:38:04 GMT+0000 (GMT)
const path = require('path');

module.exports = function(config) {
  console.log('nel karmaconfig', );
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: process.cwd() + '/client/',
    plugins: [
      "karma-chrome-launcher",
      "karma-jasmine",//,"karma-requirejs"
      "karma-spec-reporter"
    ],


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],//,'requirejs'],


    // list of files / patterns to load in the browser
    files: [
      { pattern: 'lib/bootstrap/dist/css/bootstrap.min.css', included: false, served: true, watched: false },
      { pattern: 'lib/jquery/dist/jquery.min.js', included: false, served: true, watched: false },
      { pattern: "lib/bootstrap/dist/js/bootstrap.min.js", included: false, served: true, watched: false },
      { pattern: "lib/underscore/underscore.js", included: false, served: true, watched: false },
      { pattern: "lib/angular/angular.min.js", included: true, served: true, watched: false },
      { pattern: 'lib/angular-mocks/angular-mocks.js', included: true },
      { pattern: "lib/angular-route/angular-route.min.js", included: false, served: true, watched: false },
      { pattern: "lib/angular-file-upload/dist/angular-file-upload.min.js", included: false, served: true, watched: false },
      { pattern: "lib/angular-local-storage/dist/angular-local-storage.min.js", included: false, served: true, watched: false },
      { pattern: "lib/typeahead.js/dist/typeahead.bundle.js", included: false, served: true, watched: false },
      { pattern: "lib/angular-typeahead/dist/angular-typeahead.min.js", included: false, served: true, watched: false },
      { pattern: 'views/*.html', included: false, served: true },
      { pattern: 'js/*.js', included: true, served: true },
      { pattern: 'js/controllers/*.js', included: true, served: true },
      "index.html",
      { pattern: '../test/angular/*.spec.js', included: true, served: true }
    ],
      //{ pattern: '../node_modules/angular-mocks/angular-mocks.js', included: true },


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec'],
    specReporter: {
      maxLogLines: 5,             // limit number of lines logged per test
      suppressErrorSummary: false, // do not print error summary
      suppressFailed: false,      // do not print information about failed tests
      suppressPassed: false,      // do not print information about passed tests
      suppressSkipped: true,      // do not print information about skipped tests
      showSpecTiming: true,      // print the time elapsed for each spec
      failFast: true              // test would finish with error when a first fail occurs. 
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,
    
  })
}
