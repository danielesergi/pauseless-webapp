angular.module('pauseless',['ngRoute','angularFileUpload','LocalStorageModule','siyfion.sfTypeahead'])
.config(['$routeProvider','$httpProvider', '$locationProvider',
function ($routeProvider,$httpProvider, $locationProvider)
{
  //$httpProvider.defaults.headers.common['pauseless-user']='danistick';

  $routeProvider
     .when('/readme', {controller: 'empty', templateUrl:'/views/readme.html' })
     .when('/songs', {controller: 'songs', templateUrl:'/views/canzoni.html' })
     .when('/test_page', {controller: 'test', templateUrl:'/views/test.html' })
     .when('/cds', {controller: 'cds', templateUrl:'/views/cds.html' }) 
     .when('/mixes', {controller: 'mixes', templateUrl:'/views/mixes.html' })
     .when('/tmpmixes', {controller: 'tmpmixes', templateUrl:'/views/tmpmixes.html' })
     .when('/keys', {controller: 'keys', templateUrl:'/views/keys.html' })
     .when('/cd/:id', {controller: 'cd', templateUrl:'/views/cd.html' })
     .when('/mix/:id', {controller: 'show_mix', templateUrl:'/views/show_mix.html' })
     .when('/key_mix/:id', {controller: 'mix_keys', templateUrl:'/views/show_mix_key.html' })
     .when('/song/:cd/:track', {controller: 'song', templateUrl:'/views/song.html' })//sta cosa serve ancora??? 24/10/2019
     .when('/cd', {controller: 'cds', templateUrl:'/views/cds.html' })
     .when('/cd/:id', {controller: 'cd', templateUrl:'/views/cd.html' })
     .when('/mix', {controller: 'mixes', templateUrl:'/views/mixes.html' })
     .when('/mix/:id', {controller: 'mix', templateUrl:'/views/mix.html' })
     .otherwise({redirectTo:'/cd'});

    $httpProvider.defaults.headers.put['Content-Type']='application/json';
    $httpProvider.defaults.headers.post['Content-Type']='application/json';

    /*$httpProvider.interceptors.push(function() {
      return {
       'request': function(config) {
           // same as above
        },

        'response': function(response) {
           // same as above
        }
      };
    });*/
    //$locationProvider.html5Mode(true);
    //console.log($httpProvider.defaults)
}])
.filter('nvl', function()
{
    return function(value, replacer) {
      return value ? value : (replacer ? replacer : '...');
    };
})
.filter('lowerCase', function() {
    return function(x) {
        var i, c, txt = "";
        for (i = 0; i < x.length; i++) {
            c = x[i];
            if (i % 2 == 0) {
                c = c.toLowerCase();
            }
            txt += c;
        }
        return txt;
    };
})
.filter('tempMixName', function() {
    return function(x) {
        var txt = x.replace('.xml', '').replace('tmp_', '');
        return txt;
    };
})
.factory('songsFactory', function() {
  return {
		song: function (track, artist, title, wav, bpm, key, pause) {
      if (!pause) pause = ["-"];
			return { "$":{
                    "artist":artist,
                    "track":parseInt(track),
                    "title":title,
                    "wav":wav,
                    "bpm":bpm ? bpm : '?',
                    "key":key ? key : '' },
                  "pause":pause};
		},
    cd: function (name) {
      return {"$":{"name":name},"song":[]}
    }
  }
})
.factory('unsavedState', function(localStorageService, $rootScope) {
  return {
		unsaved: function () {
      $rootScope.unsaved = true;
      localStorageService.set('unsaved', true);
		},
    reset: function () {
      localStorageService.remove('unsaved');
    }
  }
})
.run(['$rootScope','$location','$http','localStorageService',
function ($rootScope,$location,$http,localStorageService)
{
   // inizializzazione rootScope
   $rootScope.active= function (path,strict)
   {
       return (strict ? $location.path() : $location.path().substr(0, path.length)) == path;
   };

   $rootScope.dumpCd =function () {

       $http
       ({
           url: '/cd/dump',
           method: 'POST',
           data: {}
       })
       .then(function (reply)
       {
         $http
         ({
             url: '/mix/dump',
             method: 'POST',
             data: {}
         })
         .then(function (reply)
         {
         	$rootScope.unsaved = false;
          localStorageService.remove('unsaved');
         });
       });
   }

   $rootScope.unsaved = localStorageService.get('unsaved');

}]);
