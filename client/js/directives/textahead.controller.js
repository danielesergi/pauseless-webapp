angular.module('pauseless')
    .controller('textAheadController', ['$rootScope', '$scope', '$http', 
    function($rootSCope, $scope, $http) {
        $scope.aheadDataset = {};

        // Typeahead options object
        $scope.aheadOptions = {
            displayKey: 'title'
        };

        $scope.loaded = false;

        /*var numbersTemp = new Bloodhound({
            datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.num); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: [
                { num: 'one' },
                { num: 'two' },
                { num: 'three' },
                { num: 'four' },
                { num: 'five' },
                { num: 'six' },
                { num: 'seven' },
                { num: 'eight' },
                { num: 'nine' },
                { num: 'ten' }
            ]
        });
        numbersTemp.initialize();

        $scope.aheadDataset = {
            displayKey: 'num',
            source: numbersTemp.ttAdapter(),
            templates: {
                empty: [
                    '<div class="tt-suggestion tt-empty-message">',
                    'No results were found ...',
                    '</div>'
                ].join('\n'),
            }
        };*/

        $http.get($scope.dataUrl).then(function (artists) {
            // instantiate the bloodhound suggestion engine
            var numbers = new Bloodhound({
                datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.num); },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: _.map(artists.data, function(el) {return {num:el}})
            });

            // initialize the bloodhound suggestion engine
            numbers.initialize();
            //console.log(numbers.ttAdapter(), _.map(artists.data, function(el) {return {num:el}}));

            $scope.aheadDataset = {
                displayKey: 'num',
                source: numbers.ttAdapter(),
                templates: {
                    empty: [
                        '<div class="tt-suggestion tt-empty-message">',
                        'No results were found ...',
                        '</div>'
                    ].join('\n'),
                }
            };
            //NECESSARIO non far partire l'html prima che il motore abbia i dati
            $scope.loaded = true;

        });
    }]);

