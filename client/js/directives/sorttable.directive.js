angular.module('pauseless')
	.directive('sortTable', function() {
        return {
            restrict: 'E',
            templateUrl: '/js/directives/sorttable.directive.html',
            controller: 'sortTableController',
            //controllerAs: 'ctrl',
            scope: {
              elementi: '=elementi',
            }
        };
    });
