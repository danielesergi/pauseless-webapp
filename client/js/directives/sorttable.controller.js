angular.module('pauseless')
    .controller('sortTableController', ['$rootScope', '$scope',
    function($rootSCope, $scope) {
        $scope.sortField = "";
        $scope.sortDirection = 'asc';
        
        $scope.elementi.then(function(elementi){
            $scope.intestazioni = Object.keys(elementi.data[0]);
            $scope.righe = elementi.data;
        });

        $scope.clickIntestazione = function(intestazione) {
            //console.log('click su '+intestazione);
            $scope.righe = _.sortBy($scope.righe, function(riga) {
                return riga[intestazione];
            })
            if ($scope.sortField === intestazione) {
                $scope.sortDirection === 'asc' ? $scope.sortDirection = 'desc' : $scope.sortDirection = 'asc';
            } else $scope.sortDirection = 'asc';

            if ($scope.sortDirection === 'desc')
                $scope.righe.reverse();

            $scope.sortField = intestazione;
        }
    }]);

