angular.module('pauseless')
	.directive('textAhead', function() {

        return {
            restrict: 'E',
            templateUrl: '/js/directives/textahead.directive.html',
            controller: 'textAheadController',
            //controllerAs: 'ctrl',
            scope: {
              dataUrl: '=dataurl',
            },
            compile: function(element, attrs) {
                //copia tutti gli attributi html della direttiva sul tag input
                var input = element.find('input');

                // Copy attrbutes
                angular.forEach(attrs.$attr, function(val, key) {
                    //console.log(val, key)
                    input.attr(val, attrs[key]);
                });

                return;
            }
        };
    });
