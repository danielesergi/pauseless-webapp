angular.module('pauseless')
    .controller('song',['$rootScope','$scope','$http','$location','$routeParams',
        function ($rootScope,$scope,$http,$location,$routeParams) {

            $http.get('/song/'+$routeParams.cd+'/'+$routeParams.track).then(function (song)
            {
                $scope.song= song.data;
                $scope.cd = $routeParams.cd;
                $scope.pause = [];
                _.each(song.data.pause, function(el) {
                    $scope.pause.push({valore:el});
                })

            });

            $scope.updatePause = function () {
                //$scope.$apply();
                $scope.song.pause = [];
                _.each($scope.pause, function(el) {
                    $scope.song.pause.push(el.valore);
                })
                console.log("prima di spedire", $scope.song);
            }

            $scope.aggiungiPause = function () {
                $scope.pause.push({valore:''});
            }

            $scope.togliPause = function (i) {
                $scope.pause.splice(i, 1);
            }

        }]);