angular.module('pauseless')
    .controller('songs',['$rootScope','$scope','$http',
        function ($rootScope,$scope,$http) {

            $scope.songs = $http.get('/json/songs');

        }]);