angular.module('pauseless')
       .controller('keys',['$rootScope','$scope','$http','$location',
function ($rootScope,$scope,$http,$location)
{
    
    $http.get('/json/keys').then(function (keys)
    {
        $scope.keys = keys.data;

        var songs_by_key = {};

        $http.get('/json/cd').then(function (cds)
        {

            for (var cd in cds.data) {
                var songs = cds.data[cd].song;
                for (var song in songs) {
                    songs[song].$.path = cds.data[cd].$.name + "/" + songs[song].$.track;
                    if (songs_by_key[songs[song].$.key])
                        songs_by_key[songs[song].$.key].push(songs[song].$);
                    else {
                        songs_by_key[songs[song].$.key] = [];
                        songs_by_key[songs[song].$.key].push(songs[song].$);
                    }
                }
            }
            //console.log(songs_by_key);
            $scope.songs_by_key = songs_by_key;
            $scope.loaded = 1;
            
        });
    });
    
    $scope.toggle = function ($event) {
    	var key = $event.target.childNodes[0].data;
    	
    	$scope.key = key;
    	$scope.songs = $scope.songs_by_key[$event.target.childNodes[0].data];
    }

}]);
