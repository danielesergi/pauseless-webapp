angular.module('pauseless')
	.controller('mix_keys', ['$rootScope', '$scope', '$http', '$location', '$routeParams',
		function ($rootScope, $scope, $http, $location, $routeParams) {
			var songs_by_key = {};

			var key_add = function (k, scarto) { //sottrae anche...
				//console.log(k.indexOf('-'), this.get(), get_keys());
				//console.log(k, scarto)
				var keys = k.indexOf('-') > -1 ? $scope.keys.minor : $scope.keys.major,
					idx = _.findIndex(keys, function(el) { return el.key === k }) + scarto;
	
				//console.log(k, scarto, keys, idx)
	
				if (idx > 11)
					idx = idx - 12;
	
				if (idx < 0)
					idx = idx + 12;
	
				console.log(JSON.stringify(keys), idx, k, scarto);
				return keys[idx].key;
	
			}
			
			var transpose_key= function (s, bpm_m) {
				//s.$.bpm_ref = bpm_s;
  			if (bpm_m === parseInt(s.$.bpm)) {
					s.$.scarto = 0;
					s.$.transposed_key = s.$.key;
  				return;// s.$.key;
  			} else {
  				var scarto = bpm_m - parseInt(s.$.bpm);
					s.$.scarto = scarto;
  				//console.log("calcolo scarto :", bpm_m, s.$.bpm, scarto);

          //if (scarto < scarto_max)
						s.$.transposed_key = key_add(s.$.key, scarto);
						return;
          //else
            //return "k+"; //una tonalita inesistente per far fallire il test di uguaglianza
  			}
  		}

			$http.get('/json/cd').then(function (cds) {
				$http.get('/json/keys').then(function (keys) {
					$scope.keys = keys.data;

					$http.get('/json/mix/' + $routeParams.id).then(function (songs) {
						$scope.mix = songs.data;
						//console.log(songs);

						var mix_keys = {};
						songs.data.song.forEach(function (song, idx) {
							transpose_key(song, parseInt($scope.mix.$.bpm));
							//console.log(idx, song.$.transposed_key, song.$);
							song.$.index = idx;
							if (!mix_keys[song.$.transposed_key]) {
								mix_keys[song.$.transposed_key] = [];
							}
							mix_keys[song.$.transposed_key].push(song.$);
						});
						//console.log(mix_keys);

						keys.data.major.forEach(function (key) {
							if (mix_keys[key.key]) {
								key.songs = [];
								mix_keys[key.key].forEach(function (song) {
									key.songs.push(song);
								});
							}
						});
						keys.data.minor.forEach(function (key) {
							if (mix_keys[key.key]) {
								key.songs = [];
								mix_keys[key.key].forEach(function (song) {
									key.songs.push(song);
								});
							}
						});


						//console.log(keys);
						$scope.mix_name = $routeParams.id;
						$scope.loaded = 1;
					});

				});



			});


		}]);

