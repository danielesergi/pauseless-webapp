angular.module('pauseless')
	.controller('cd', ['$rootScope', '$scope', '$http', '$location', '$routeParams', 'FileUploader', 'songsFactory', 'unsavedState',
		function ($rootScope, $scope, $http, $location, $routeParams, FileUploader, songsFactory, unsavedState) {
			$scope.xml_toggle = false;
			$scope.edit_toggle = true;
			$scope.files = [];
			$scope.song = {};
			$scope.pause = [];
			$scope.loadedPage = false;

			$scope.options = {
				formato: [{ val: 'wav', nome: 'wav' }, { val: 'mp3', nome: 'mp3' }],
				keys: []
			}

			/*$scope.numbersDataset = {};

			// Typeahead options object
			$scope.exampleOptions = {
				displayKey: 'title'
			};*/

			$http.get('/json/cd/' + $routeParams.id).then(function (cd) {
				$scope.songs = cd.data.song;

				$http.get('/json/keys').then(function (keys) {
					var ke = [];

					keys.data.major.forEach(function (el) { ke.push(el); $scope.options.keys.push({ val: el.key, nome: el.key }) });
					keys.data.minor.forEach(function (el) { ke.push(el); $scope.options.keys.push({ val: el.key, nome: el.key }) });

					$scope.keys = ke;
					$scope.track = cd.data.song ? cd.data.song.length + 1 : 1;
				});
			});

			//finche la pagina non carica
			/*var numbersTemp = new Bloodhound({
				datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.num); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: [
					{ num: 'one' },
					{ num: 'two' },
					{ num: 'three' },
					{ num: 'four' },
					{ num: 'five' },
					{ num: 'six' },
					{ num: 'seven' },
					{ num: 'eight' },
					{ num: 'nine' },
					{ num: 'ten' }
				]
			});
			numbersTemp.initialize();*/

			/*$http.get('/json/artists').then(function (artists) {
				// instantiate the bloodhound suggestion engine
				var numbers = new Bloodhound({
					datumTokenizer: function (d) { return Bloodhound.tokenizers.whitespace(d.num); },
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: _.map(artists.data, function(el) {return {num:el}})
				});

				// initialize the bloodhound suggestion engine
				numbers.initialize();
				//console.log(numbers.ttAdapter(), _.map(artists.data, function(el) {return {num:el}}));

				$scope.numbersDataset = {
					displayKey: 'num',
					source: numbers.ttAdapter(),
					templates: {
						empty: [
							'<div class="tt-suggestion tt-empty-message">',
							'No results were found ...',
							'</div>'
						].join('\n'),
					}
				};
				//NECESSARIO non far partire l'html prima che il motore abbia i dati
				$scope.loadedPage = true;

			});*/

			$scope.cd_name = $routeParams.id;
			$scope.wav = "wav";

			$scope.addSong = function () {
				if ($scope.artist && $scope.track && $scope.title) {
					var s = songsFactory.cd($routeParams.id);

					//passargli il numero di traccia è ora inutile
					song = songsFactory.song($scope.songs.length + 1, $scope.artist, $scope.title, $scope.wav, $scope.bpm);
					s.song.push(song);

					$http
						({
							url: '/cd/song/add',
							method: 'POST',
							data: JSON.stringify(s)
						})
						.then(function (cd) {
							$scope.artist = "";
							$scope.title = "";
							$scope.key = "";
							$scope.wav = "wav";
							$scope.bpm = "";
							$scope.songs = cd.data.song;
							$scope.track = cd.data.song.length + 1;
							unsavedState.unsaved();
						});
				}
			}

			$scope.updateSong = function (song) {
				console.log(song);
				var s = {
					cd: $routeParams.id, song: {
						"$": {
							"artist": song.$.artist,
							"track": song.$.track,
							"title": song.$.title,
							"wav": song.$.wav,
							"bpm": song.$.bpm,
							"key": song.$.key
						},
						"pause": song.pause
					}
				};
				$http
					({
						url: '/cd/song/update',
						method: 'POST',
						data: JSON.stringify(s)
					})
					.then(function (cd) {
						$scope.songs = cd.data.song;
						unsavedState.unsaved();
					});
			}

			$scope.deleteSong = function (song) {
				var s = {
					cd: $routeParams.id, song: {
						"$": {
							"track": song.$.track,
						}
					}
				};
				$http
					({
						url: '/cd/song/delete',
						method: 'POST',
						data: JSON.stringify(s)
					})
					.then(function (cd) {
						$scope.songs = cd.data.song;
						unsavedState.unsaved();
					});
			}

			$scope.swapSong = function (from, to) {
				var s = {
					cd: $routeParams.id,
					from: from,
					to: to
				};
				$http
					({
						url: '/cd/song/swap',
						method: 'POST',
						data: JSON.stringify(s)
					})
					.then(function (cd) {
						$scope.songs = cd.data.song;
						unsavedState.unsaved();
					});
			}

			$scope.show_xml = function () {
				if (!$scope.xml_toggle) {
					//if (!$scope.xml_cd)
					$http
						({
							url: '/xml/cd/' + $routeParams.id,
							method: 'GET'
						})
						.then(function (reply) {
							$scope.xml_cd = reply.data.split(" used=\"0\"").join("");
						});

					$scope.xml_toggle = true;
				} else {
					$scope.xml_toggle = false;
				}
			}

			$scope.toggle_edit = function () {
				$scope.edit_toggle = !$scope.edit_toggle
			}
			/*
				$scope.show_inline_edit= function(song) {
					if(!song.$.inline_edit) {
						song.$.inline_edit = true;
					} else {
						song.$.inline_edit = false;
					}
				}*/

			var uploader = $scope.uploader = new FileUploader({
				url: 'upload.php'
			});

			uploader.onAfterAddingFile = function (fileItem) {
				console.info('onAfterAddingFile', fileItem.file.name);
				$scope.files.push({ name: fileItem.file.name });
				$scope.files = _.sortBy($scope.files, 'name');
			};

			uploader.filters.push({
				name: 'songFilter',
				fn: function (item /*{File|FileLikeObject}*/, options) {
					var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
					return true;//'|wav|mp3|flac|'.indexOf(type) !== -1;
				}
			});

			$scope.add_files_to_cd = function () {
				$scope.songs = [];
				var s = songsFactory.cd($routeParams.id);

				_.each($scope.files, function (file, idx) {
					name = file.name.split(".")[0].toLowerCase();
					//console.log(name);

					song = songsFactory.song(name.split(" - ")[0], name.split(" - ")[1], name.split(" - ")[2], "wav");
					//console.log(song);
					s.song.push(song);
				});

				$http
					({
						url: '/cd/song/add',
						method: 'POST',
						data: JSON.stringify(s)
					})
					.then(function (reply) {
						$http.get('/json/cd/' + $routeParams.id).then(function (songs) {
							$scope.songs = songs.data;
							$scope.track = $scope.songs.length + 1;
							$scope.files = [];
						});
						unsavedState.unsaved();
					});

			};

			$scope.showSong = function (s) {
				$location.path('/#/song/' + $routeParams.id + '/' + s.$.track);
			}

			$scope.openPause = function (s, idx) {
				$rootScope.pause = s.pause;

				var modalInstance = $uibModal.open({
					animation: $ctrl.animationsEnabled,
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: 'myModalContent.html',
					controller: 'ModalInstanceCtrl',
					controllerAs: '$ctrl',
					size: size,
					appendTo: parentElem,
					resolve: {
						items: function () {
							return $ctrl.items;
						}
					}
				});
				$("#myModal").modal();

				$("#myModal").on('hide.bs.modal', function () {
					//alert('The modal is about to be hidden.');
					//delete $scope.pause;
				});

				$scope.updatePause = function () {
					console.log('qui', idx)
					console.log('qui', $scope.songs[idx].pause)
					console.log('qui', $rootScope.pause)
					$scope.songs[idx].pause = $rootScope.pause;
					$("#myModal").modal("hide");
				}
			}


			//console.info('uploader', uploader);

		}]);


//{"$":{"name":"da 3"},"song":[{"$":{"track":"1","bpm":"145","artist":"2012 vs. tera","wav":"mp3","key":"D#+","title":"high paradise","used":1,"used_mixes":["dicembre2012"]},"pause":["3.00-2.20"]},{"$":{"track":"2","bpm":"148","artist":"sulima","wav":"mp3","key":"A+","title":"between galaxies","used":0},"pause":["5.25-5.12","4.20-4.06","3.28-3.02","2.10-1.29"]},{"$":{"track":"3","bpm":"144","artist":"burn in noise vs headroom","wav":"wav","key":"A+","title":"frequency salad","used":1,"used_mixes":["best"]},"pause":["3.12-2.06"]},{"$":{"track":"4","bpm":"144","artist":"sunday light","wav":"wav","key":"G-","title":"21st century advances","used":0},"pause":["3.46-2.44"]},{"$":{"track":"5","bpm":"142","artist":"amd","wav":"wav","key":"C+","title":"morning glory","used":0},"pause":["6.25-6.11"]},{"$":{"track":"6","bpm":"145","artist":"xi","wav":"wav","key":"A#-","title":"elements - original mix","used":0},"pause":["4.23-3.53","2.33-2.07"]},{"$":{"track":"7","bpm":"145","artist":"killerwatts vs waio","wav":"wav","key":"A#+","title":"wake up","used":1,"used_mixes":["best"]},"pause":["2.47-1.52"]},{"$":{"track":"8","bpm":"143","artist":"man machine","wav":"wav","key":"G#-","title":"subatomic","used":1,"used_mixes":["febbraio2014"]},"pause":["6.04-5.44","3.56-2.22"]},{"$":{"track":"9","bpm":"147","artist":"lyric","wav":"mp3","key":"E+","title":"no gravity","used":0},"pause":["5.12-4.58","4.06-3.27","2.09-1.22"]}]}
