angular.module('pauseless')
       .controller('cds',['$rootScope','$scope','$http','$location','$routeParams','songsFactory','unsavedState',
function ($rootScope,$scope,$http,$location,$routeParams, songsFactory,unsavedState)
{

  $scope.init = function () {
    $http.get('/json/cd').then(function (cds)
    {
        $scope.cds= cds.data ? cds.data : [];
    });
  }

  $scope.init();

  $scope.addCd =function () {
  	var cd =songsFactory.cd($scope.cdName);
  	//$scope.cds.push(cd);
	  $scope.cdName = "";

      $http
      ({
          url: '/cd/add',
          method: 'POST',
          data: JSON.stringify(cd)
      })
      .then(function (cds)
      {
        $scope.cds = cds.data;
        unsavedState.unsaved();
      });
  }

  /* $scope.dumpCd =function () {

      $http
      ({
          url: '/cd/dump',
          method: 'POST',
          data: {}
      })
      .success(function (reply)
      {
      	//console.log('server',reply);
      });
  } */

}]);
