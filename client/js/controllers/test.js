angular.module('pauseless')
       .controller('test',['$rootScope','$scope','$http','$location',
function ($rootScope,$scope,$http,$location)
{
    /*funzioni prese dai services dell'app ionic*/
    function key_add (k, scarto) { //sottrae anche...
        var keys = k.indexOf('-') > -1 ? get_keys().minor : get_keys().major,
            idx = _.findIndex(keys, function(el) { return el.key === k }) + scarto;

        if (idx > 11)
            idx = idx - 12;

        if (idx < 0)
            idx = idx + 12;

        //console.log(JSON.stringify(keys), idx, k, scarto);
        return keys[idx].key;

    }

    function get_key_m (k) {
        var keys = k.indexOf('-') > -1 ? get_keys().minor : get_keys().major,
            mark = k.indexOf('-') > -1 ? 'a' : 'b',
            idx = _.findIndex(keys, function(el) { return el.key === k });

        return (k.indexOf('-') > -1 ? get_keys().major : get_keys().minor)[idx].key;
    }

    function transpose_key (s, bpm) {
        var scarto_max = 3;
        if (bpm === parseInt(s.$.bpm))
            return s.$.key;
        else {
            var scarto = bpm - parseInt(s.$.bpm);
            //console.log("calcolo scarto :", bpm, s.$.bpm, scarto, scarto_max);

            if (scarto < scarto_max) //TODO : parametrizzare lo scarto
                return key_add(s.$.key, scarto);
            else
                return "k+"; //una tonalita inesistente per far fallire il test di uguaglianza
        }
    }

    function get_keys () {
        var keys = {};
        keys.minor = [];
        keys.major = [];
        keys.minor.push({key:"G#-", position:1, mark:"a"});
        keys.minor.push({key:"D#-", position:2, mark:"a"});
        keys.minor.push({key:"A#-", position:3, mark:"a"});
        keys.minor.push({key:"F-", position:4, mark:"a"});
        keys.minor.push({key:"C-", position:5, mark:"a"});
        keys.minor.push({key:"G-", position:6, mark:"a"});
        keys.minor.push({key:"D-", position:7, mark:"a"});
        keys.minor.push({key:"A-", position:8, mark:"a"});
        keys.minor.push({key:"E-", position:9, mark:"a"});
        keys.minor.push({key:"B-", position:10, mark:"a"});
        keys.minor.push({key:"F#-", position:11, mark:"a"});
        keys.minor.push({key:"C#-", position:12, mark:"a"});

        keys.major.push({key:"B+", position:1, mark:"b"});
        keys.major.push({key:"F#+", position:2, mark:"b"});
        keys.major.push({key:"C#+", position:3, mark:"b"});
        keys.major.push({key:"G#+", position:4, mark:"b"});
        keys.major.push({key:"D#+", position:5, mark:"b"});
        keys.major.push({key:"A#+", position:6, mark:"b"});
        keys.major.push({key:"F+", position:7, mark:"b"});
        keys.major.push({key:"C+", position:8, mark:"b"});
        keys.major.push({key:"G+", position:9, mark:"b"});
        keys.major.push({key:"D+", position:10, mark:"b"});
        keys.major.push({key:"A+", position:11, mark:"b"});
        keys.major.push({key:"E+", position:12, mark:"b"});

        return keys;
    }
    /*funzioni prese dai services dell'app ionic*/

    $scope.test = "ciao mondo";

    $scope.initialMix = [{
        "$": {
            "path": "da 63/7",
            "note": "bassa rif 2.00",
            "song": "sheenigami shapeshifter (145)",
            "key": "G+",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 55/2",
            "note": "no seconda pausa",
            "song": "spirit architect and djantrix full moon (145)",
            "key": "G+",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 61/10",
            "note": "rif 2.05",
            "song": "spirit architect and imaginary sight and djantrix pineal gland (145)",
            "key": "G+",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 18/6",
            "note": "rif 1.49",
            "song": "sybarite infinite vibe (144)",
            "key": "G+",
            "bpm": "144"
        }
    }, {
        "$": {
            "path": "da 33/1",
            "note": "Rif 2.37",
            "song": "tristan and nigel purple ohm (145)",
            "key": "G+",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 44/4",
            "note": "entra con gli alti cambio lungo rif 1.43",
            "song": "braincell the experiment (146)",
            "key": "G+",
            "bpm": "146"
        }
    }, {
        "$": {
            "path": "da 60/10",
            "note": "no seconda pausa",
            "song": "u-recken clouds of smoke (145)",
            "key": "E-",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 9/1",
            "note": "No pausa",
            "song": "hypnoacoustics ashtamangala (144)",
            "key": "E-",
            "bpm": "144"
        }
    }, {
        "$": {
            "path": "da 29/4",
            "note": "lasciala fino a 1 min",
            "song": "e-mov cenote (140)",
            "key": "B-",
            "bpm": "140"
        }
    }, {
        "$": {
            "path": "da 54/10",
            "note": "no pausa",
            "song": "spec3 evolution (145)",
            "key": "B-",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 63/3",
            "note": "bassa rif 1.55",
            "song": "tropical bleyage mala (145)",
            "key": "B-",
            "bpm": "145"
        }
    }, {
        "$": {
            "path": "da 51/3",
            "note": "",
            "song": "hypnoise vs ital the awakening (142)",
            "key": "B-",
            "bpm": "142"
        }
    }];

    var bpm = 145;
    var first_song = $scope.initialMix[2];
    //console.log(first_song);

    function rateSongs (ref_key, keys) {
        var thisMajorMinor, otherMajorMinor;

        //individuo se la "striscia" su cui lavoro è quella maggiore o minore e metto l'altra in una variabile "other"
        if (ref_key.indexOf('+') > -1) {
            thisMajorMinor = keys.major;
            otherMajorMinor = keys.minor;
        } else {
            thisMajorMinor = keys.minor;
            otherMajorMinor = keys.major;
        }

        var strip = [];
        //costruisco la "striscia".

        var centralIndex = _.findIndex(thisMajorMinor, {key:ref_key});
        strip[0] = thisMajorMinor[centralIndex];

        for (var i = -6; i < 7; i++) {
            console.log(i, key_add(ref_key, i), _.findIndex(thisMajorMinor, {key:key_add(ref_key, i)}));
            strip[i] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, i)})];
        }

/*        console.log(_.keys(thisMajorMinor));
        var centralIndex = _.findIndex(thisMajorMinor, {key:ref_key});
        console.log(centralIndex);
        strip[0] = thisMajorMinor[centralIndex];
        strip[1] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 1)})];
        strip[2] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 2)})];
        strip[3] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 3)})];
        strip[4] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 4)})];
        strip[5] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 5)})];
        strip[6] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, 6)})];

        strip[-1] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -1)})];
        strip[-2] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -2)})];
        strip[-3] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -3)})];
        strip[-4] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -4)})];
        strip[-5] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -5)})];
        strip[-6] = thisMajorMinor[_.findIndex(thisMajorMinor, {key:key_add(ref_key, -6)})];*/
//        strip[-1] = thisMajorMinor[centralIndex-1];
//        strip[1] = thisMajorMinor[centralIndex+1];
        console.log(strip);
        console.log(_.keys(strip));
    }

    $http.get('/json/keys').success(function (keys)
    {
        $scope.keys = keys;

        //$http.get('/json/mix/'+$routeParams.id).success(function (songs)
        //{
            songs= $scope.initialMix;//songs;
            //console.log(songs);

            var mix_keys = {};
            songs.forEach(function(song) {
                if (! mix_keys[song.$.key]) {
                    mix_keys[song.$.key] = [];
                }

                song.$.transposed_key = transpose_key(song, bpm);

                mix_keys[song.$.key].push(song.$);
            });
            //console.log(mix_keys);

            keys.major.forEach(function(key){
                if (mix_keys[key.key]) {
                    key.songs = [];
                    mix_keys[key.key].forEach(function(song){
                        key.songs.push(song);
                    });
                }
            });
            keys.minor.forEach(function(key){
                if (mix_keys[key.key]) {
                    key.songs = [];
                    mix_keys[key.key].forEach(function(song){
                        key.songs.push(song);
                    });
                }
            });
        console.log(keys);

        //TODO: fare un transpose di tutte le chiavi della playlist (per ora bpm fissi)
        rateSongs(first_song.$.key, keys);

            //console.log(keys);
            //$scope.mix_name = $routeParams.id;
            $scope.loaded = 1;
        //});

    });

}]);
