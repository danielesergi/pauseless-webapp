angular.module('pauseless')
       .controller('tmpmixes',['$rootScope','$scope','$http','unsavedState',
function ($rootScope,$scope,$http,unsavedState)
{

  function init() {
    $http.get('/json/tmpmix').then(function (mixes)
    {
        $scope.mixes= mixes.data ? mixes.data : [];
    });
  }
  init();

  $scope.dumpMix = function(mix) {
    $http.get('/tmpmix/save/'+mix).then(function ()
    {
        unsavedState.unsaved();
        $scope.removeMix(mix);
    });
  }

  $scope.removeMix = function(mix) {
    $http.get('/tmpmix/remove/'+mix).then(function ()
    {
        init();
    });
  }

}]);
