angular.module('pauseless')
       .controller('mix',['$rootScope','$scope','$http','$routeParams',
function ($rootScope,$scope,$http,$routeParams)
{
  $http.get('/json/mix/'+$routeParams.id).then(function (mix)
  {
      $scope.songs= mix.data.song;
      $scope.mix_name = mix.data.$.name;
      $scope.mix_bpm = mix.data.$.bpm;
      $scope.mix_description = mix.data.$.description;
  });


}]);
