# Pauseless webapp

Questa app è studiata come parte di web editing per la lista dei cd/mix da usare nell'app Pauseless sul cellulare

## Documentazione test

I test sono di due tipi : NONSOCOMESICHIAMANO eseguiti da mocha e lanciati tramite Gulp
```
gulp
```

e NONSOCOMESICHIAMANO eseguiti da karma e lanciati sempre tramite gulp
```
gulp karma
```

entrambi i comandi sono semplicemente definiti nel gulpfile.js. Non so perchè, ma installare normalmente Gulp con npm install non mi da accesso all'eseguibile, quindi in una nuova shell bisogna innanzitutto lanciare il comando
```
alias gulp=node_modules/.bin/gulp
```

### lanciare un singolo test mocha

modificare il nome del file nella funzione single_test all'interno del gulpfile.js, poi lanciare
```
gulp single
```
